//  CapekMath class for Capek application
//  By Paul Andrews November 2018
//

"use strict";

class CapekMath {

    static modulo(aNum, aDiv) {
        // Supports negatives, which js operator % does not
        return ((aNum % aDiv) + aDiv) % aDiv
    }

}

module.exports = {CapekMath};
