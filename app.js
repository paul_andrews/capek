//  App class for Capek application
//  By Paul Andrews November 2018
//

"use strict";

var Table = require('./table.js').Table;
var Robot = require('./robot.js').Robot;
var Stdin = require('./stdin.js').Stdin;

class App {

    constructor() {
        this.table = new Table(5, 5);
    }

    run() {
        this.robot = new Robot();
        new Stdin(
            this.onCommand.bind(this),
            this.onClose.bind(this)
        );
    }

    onCommand(aCommand) {
        var cmdRes
        cmdRes = this.robot.doCommand(aCommand);
        if ( cmdRes )
            log(cmdRes);
    }

    onClose() {
        process.exit();
    }

}

global.app = new App();
global.log = console.log;

module.exports = {App};
