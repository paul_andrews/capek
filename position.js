//  Position class for Capek application
//  By Paul Andrews November 2018
//

"use strict";

class Position {

    constructor(aX, aY) {
        this.x = aX;
        this.y = aY;
    }

    getAdjacent(aDir) {
        var res = this.copy();
        var inc = aDir.getSign(); // +1 for north/east; -1 for south/west
        if ( aDir.isVertical() )
            res.y = res.y + inc;
        else
            res.x = res.x + inc;
        return res;
    }

    copy() {
        return new Position(this.x, this.y);
    }

}

module.exports = {Position};
