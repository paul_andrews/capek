# Capek

Capek is an application that simulates a robot moving on a table in response to text commands.

By Paul Andrews November 2018.

## Prerequisites

Node 8.12.0 or later. (All testing performed with node 8.12.0).

## Installation

Copy the files to any folder.

## Usage

To run the tests:
    node capek_tester.js

To create and control a simulated robot with text commands read from standard input:
    node capek.js

