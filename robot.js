//  Robot class for Capek application
//  By Paul Andrews November 2018
//

"use strict";

var App = require('./app.js').App;
var Position = require('./position.js').Position;
var Parser = require('./parser.js').Parser;

class Robot {

    place(aX, aY, aFacing) {
        var p = new Position(aX, aY);
        if ( ! app.table.hasPosition(p) )
            return "Out of bounds";
        if ( ! aFacing )
            return "Invalid facing direction";
        this.position = p;
        this.facing = aFacing;
    }

    move() {
        var p = this.position;
        if ( ! p ) 
            return;
        var newP = p.getAdjacent(this.facing);
        if ( ! app.table.hasPosition(newP) )
            return;
        this.position = newP;
    }

    left() {
        var f = this.facing
        if ( ! f )
            return null;
        this.facing = f.rotateLeft();
    }

    right() {
        var f = this.facing
        if ( ! f )
            return null;
        this.facing = f.rotateRight();
    }

    report() {
        var p = this.position;
        var f = this.facing;
        if ( (! p) || (! f) ) 
            return;
        return p.x + "," + p.y + "," + f.compassPoint;
    }

    doCommand(aCmd) {
        var f = Parser.commandToFunction(aCmd, this);
        if ( ! f ) 
            return "Invalid Command: " + aCmd;
        var res = f();
        return res;
    }

}

module.exports = {Robot};
