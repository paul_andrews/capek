//  Parser class for Capek application
//  By Paul Andrews November 2018
//

"use strict";

var Direction = require('./direction.js').Direction;

class Parser {

    static commandToFunction(aCommand, aRobot) { 
        var word = this.commandToFirstWord(aCommand);
        if ( word == "PLACE" )
            return this.commandToPlaceFunction(aCommand, aRobot);
        else if ( ["MOVE", "LEFT", "RIGHT", "REPORT"].indexOf(word) >= 0 )
            return this.commandToFunctionWithNoArgs(word, aCommand, aRobot);
    }

    static commandToFirstWord(aCommand) {
        if ( ! aCommand )
            return "";
        var arr = aCommand.split(" ");
        return ( arr.length > 0 ) ? arr[0] : "";
    }

    static commandToFunctionWithNoArgs(aWord, aCommand, aRobot) {
        if ( aWord != aCommand )
            return null;
        var fn = aWord.toLowerCase();
        return eval("aRobot." + fn + ".bind(aRobot)");
    }

    static commandToPlaceFunction(aCommand, aRobot) {
        var args = this.placeCommandToArgs(aCommand);
        if ( ! args )
            return null;
        var dir = Direction.createIfValid(args.compassPoint);
        return eval("aRobot.place.bind(aRobot, args.x, args.y, dir)");
    }

    static placeCommandToArgs(aCommand) {
        var res = {};
        var args = aCommand.split(/[ ,]+/); /* split by whitespace or comma */
        if ( args.length != 4 )
            return null;
        res.x = parseInt(args[1], 10);
        res.y = parseInt(args[2], 10);
        res.compassPoint = args[3].trim();
        return res;
    }

    static commandToArgStr(aCommand) {
        if ( ! aCommand )
            return "";
        var arr = aCommand.split(" ");
        return ( arr.length > 1 ) ? arr[1] : "";
    }

}

module.exports = {Parser};
