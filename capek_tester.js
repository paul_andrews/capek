//  Capek Tester
//  By Paul Andrews November 2018
//
//  Usage: node capek_tester.js

"use strict";

var Robot = require('./robot.js').Robot;
var Direction = require('./direction.js').Direction;
var Position = require('./position.js').Position;

const NORTH = Direction.north();
const EAST = Direction.east();
const SOUTH = Direction.south();
const WEST = Direction.west();

class CapekTester {

    runTests() {
        var succeeded = true;
        this.testCount = 0;
        try {
            this.runUnitTests();
            this.runFunctionalTests();
        } catch(e) {
            succeeded = false;
            log(e.message);
        }
        if ( succeeded )
            log("Finished testing - " + this.testCount + " tests passed");
    }

    runUnitTests() {
        this.testPlace();
        this.testMove();
        this.testLeft();
        this.testRight();
        this.testDoesntFallOff();
        this.testReport();
        this.testCommandsThatShouldBeIgnored();
        this.testTextCommandProcessing();
        this.testInvalidTextCommandHandling();
    }

    runFunctionalTests() {
        this.testWithTestCasesFromSpec();
        this.testWithFileInput();
    }

    testPlace() {
        this.onNewTestStart();
        var r = new Robot();
        var p;
        p = r.position;
        this.assert(p == null, "Robot shouldn't be on table until it has been placed");
        r.place(-1, 5, SOUTH);
        p = r.position;
        this.assert(p == null, "Robot is on table but initial place call was invalid");
        r.place(0, 0, NORTH);
        p = r.position;
        this.assert((p.x == 0) && (p.y == 0) & (r.facing.equals(NORTH)), "Robot placement failed");
        r.place(3, 2, SOUTH);
        p = r.position;
        this.assert((p.x == 3) && (p.y == 2) & (r.facing.equals(SOUTH)), "Robot placement failed (2)");
        r.place(5, 2, SOUTH);
        p = r.position;
        this.assert((p.x == 3) && (p.y == 2) & (r.facing.equals(SOUTH)), "Robot placement changed after place called with invalid coordinates");
        r.place(2, 5, SOUTH);
        p = r.position;
        this.assert((p.x == 3) && (p.y == 2) & (r.facing.equals(SOUTH)), "Robot placement changed after place called with invalid coordinates (2)");
        r.place(-1, 5, SOUTH);
        p = r.position;
        this.assert((p.x == 3) && (p.y == 2) & (r.facing.equals(SOUTH)), "Robot placement changed after place called with invalid coordinates (3)");
        r.place(2, -1, SOUTH);
        p = r.position;
        this.assert((p.x == 3) && (p.y == 2) & (r.facing.equals(SOUTH)), "Robot placement changed after place called with invalid coordinates (4)");
    }

    testMove() {
        this.onNewTestStart();
        var r = new Robot();
        var p
        r.place(0, 0, NORTH);
        r.move();
        p = r.position;
        this.assert((p.x == 0) && (p.y == 1), "Robot move failed");
        r.place(2, 3, SOUTH);
        r.move();
        p = r.position;
        this.assert((p.x == 2) && (p.y == 2), "Robot move failed (2)");
        r.place(3, 1, EAST);
        r.move();
        p = r.position;
        this.assert((p.x == 4) && (p.y == 1), "Robot move failed (3)");
        r.place(4, 2, WEST);
        r.move();
        p = r.position;
        this.assert((p.x == 3) && (p.y == 2), "Robot move failed (4)");
    }

    testLeft() {
        this.onNewTestStart();
        var r = new Robot();
        r.place(0, 0, NORTH);
        r.left();
        this.assert(r.facing.equals(WEST), "Robot rotate left failed");
        r.left();
        this.assert(r.facing.equals(SOUTH), "Robot rotate left failed (2)");
        r.left();
        this.assert(r.facing.equals(EAST), "Robot rotate left failed (3)");
        r.left();
        this.assert(r.facing.equals(NORTH), "Robot rotate left failed (4)");
    }

    testRight() {
        this.onNewTestStart();
        var r = new Robot();
        r.place(0, 0, NORTH);
        r.right();
        this.assert(r.facing.equals(EAST), "Robot rotate right failed");
        r.right();
        this.assert(r.facing.equals(SOUTH), "Robot rotate right failed (2)");
        r.right();
        this.assert(r.facing.equals(WEST), "Robot rotate right failed (3)");
        r.right();
        this.assert(r.facing.equals(NORTH), "Robot rotate left failed (4)");
    }

    testDoesntFallOff() {
        this.onNewTestStart();
        var r = new Robot();
        var p
        r.place(0, 0, SOUTH);
        r.move();
        p = r.position;
        this.assert((p.x == 0) && (p.y == 0), "Robot fall off check failed");
        r.place(0, 0, WEST);
        r.move();
        p = r.position;
        this.assert((p.x == 0) && (p.y == 0), "Robot fall off check failed (2)");
        r.place(4, 0, EAST);
        r.move();
        p = r.position;
        this.assert((p.x == 4) && (p.y == 0), "Robot fall off check failed (3)");
        r.place(0, 4, NORTH);
        r.move();
        p = r.position;
        this.assert((p.x == 0) && (p.y == 4), "Robot fall off check failed (4)");
    }
    
    testCommandsThatShouldBeIgnored() {
        this.onNewTestStart();
        var r = new Robot();
        this.assert((r.position == null) && (r.facing == null), "Robot has position or facing direction before being placed");
        r.move();
        this.assert((r.position == null) && (r.facing == null), "Robot failed to ignore command before being placed");
        r.left();
        this.assert((r.position == null) && (r.facing == null), "Robot failed to ignore command before being placed (2)");
        r.right();
        this.assert((r.position == null) && (r.facing == null), "Robot failed to ignore command before being placed (3)");
        this.assert(r.report() == null, "Robot failed to ignore command before being placed (4)");
    }

    testReport() {
        this.onNewTestStart();
        var r = new Robot();
        r.place(0, 0, EAST);
        this.assert(r.report() == "0,0,EAST", "Robot report was incorrect");
        r.move();
        this.assert(r.report() == "1,0,EAST", "Robot report was incorrect (2)");
        r.left();
        this.assert(r.report() == "1,0,NORTH", "Robot report was incorrect (3)");
        r.left();
        this.assert(r.report() == "1,0,WEST", "Robot report was incorrect (4)");
        r.left();
        this.assert(r.report() == "1,0,SOUTH", "Robot report was incorrect (5)");
    }

    testTextCommandProcessing() {
        this.onNewTestStart();
        var r = new Robot();
        r.doCommand("PLACE 4,4,WEST");
        this.assert(r.report() == "4,4,WEST", "Text command processing failed");
        r.doCommand("MOVE");
        this.assert(r.report() == "3,4,WEST", "Text command processing failed (2)");
        r.doCommand("LEFT");
        this.assert(r.report() == "3,4,SOUTH", "Text command processing failed (3)");
        r.doCommand("RIGHT");
        this.assert(r.report() == "3,4,WEST", "Text command processing failed (4)");
        this.assert(r.doCommand("REPORT") == "3,4,WEST", "Text command processing failed (5)");
    }

    testInvalidTextCommandHandling() {
        this.onNewTestStart();
        var r = new Robot();
        r.place(0, 0, EAST);
        this.assert(r.report() == "0,0,EAST", "Robot report was incorrect");
        r.doCommand("HJGJHGJ");
        this.assert(r.report() == "0,0,EAST", "Robot state changed after invalid command");
        r.doCommand("MOVE X");
        this.assert(r.report() == "0,0,EAST", "Robot state changed after invalid command (1)");
        r.doCommand("LEFT X");
        this.assert(r.report() == "0,0,EAST", "Robot state changed after invalid command (2)");
        r.doCommand("RIGHT X");
        this.assert(r.report() == "0,0,EAST", "Robot state changed after invalid command (3)");
        r.doCommand("REPORT X");
        this.assert(r.report() == "0,0,EAST", "Robot state changed after invalid command (4)");
        r.doCommand("move");
        this.assert(r.report() == "0,0,EAST", "Robot state changed after invalid command (5)");
        r.doCommand("PLACE 4,4,WEST,X");
        this.assert(r.report() == "0,0,EAST", "Robot state changed after invalid command (6)");
        r.doCommand("");
        this.assert(r.report() == "0,0,EAST", "Robot state changed after invalid command (7)");
        r.doCommand("PLACE XXX,4,WEST");
        this.assert(r.report() == "0,0,EAST", "Robot state changed after invalid command (8)");
        r.doCommand("PLACE 4,XXX,WEST");
        this.assert(r.report() == "0,0,EAST", "Robot state changed after invalid command (9)");
        r.doCommand("PLACE 4,4,west");
        this.assert(r.report() == "0,0,EAST", "Robot state changed after invalid command (10)");
    }

    testWithTestCasesFromSpec() {
        this.onNewTestStart();
        var res
        res = this.runAppWithInput(
            "PLACE 0,0,NORTH\n" +
            "MOVE\n" +
            "REPORT"
        );
        this.assert(res == "0,1,NORTH\n", "Test case (a) from spec failed");
        res = this.runAppWithInput(
            "PLACE 0,0,NORTH\n" +
            "LEFT\n" +
            "REPORT"
        );
        this.assert(res == "0,0,WEST\n", "Test case (b) from spec failed");
        res = this.runAppWithInput(
            "PLACE 1,2,EAST\n" +
            "MOVE\n" +
            "MOVE\n" +
            "LEFT\n" +
            "MOVE\n" +
            "REPORT"
        );
        this.assert(res == "3,3,NORTH\n", "Test case (c) from spec failed");
    }

    testWithFileInput() {
        this.onNewTestStart();
        for ( var i = 0; i < 3; i++ ) {
            this.testWithFileNo(i);
        }
    }

    testWithFileNo(aNo) {
        var fn = "./test_data/input" + aNo;
        var expectedFn = "./test_data/expectedOutput" + aNo;
        var res = this.runAppWithFileInput(fn);
        var expected = this.fileToContents(expectedFn);
        this.assert(res == expected, "Test with file input failed on test file " + fn);
    }

    runAppWithInput(aInput) {
        var cpModule = require('child_process');
        var buf = cpModule.execSync('node capek.js', {input: aInput});
        return buf.toString();
    }

    runAppWithFileInput(aInputFile) {
        var cpModule = require('child_process');
        var buf = cpModule.execSync('node capek.js < ' + aInputFile);
        return buf.toString();
    }

    fileToContents(aFile) {
        var fs = require('fs');
        return fs.readFileSync(aFile, 'utf8');
    }

    onNewTestStart() {
        this.testCount++;
    }

    assert(aCondition, aFailMsg) {
        if ( ! aCondition )
            this.fail(aFailMsg);
    }

    fail(aMsg) {
        throw new Error("TEST FAILED: " + aMsg);
    }

}

var t = new CapekTester();
t.runTests();
