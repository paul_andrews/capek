//  Stdin class for Capek application
//  By Paul Andrews November 2018
//

"use strict";


class Stdin {

    constructor(aOnRead, aOnClose) {
        var m = require('readline');
        var reader = m.createInterface({input: process.stdin});

        reader.on('line',
            function(aLine) {
                try {
                    aOnRead(aLine);
                } catch(e) {
                    log("Unexpected error: " + e.message);
                }
            }
        )

        reader.on('close',
            aOnClose
        )

    }

}

module.exports = {Stdin};
