//  Table class for Capek application
//  By Paul Andrews November 2018
//

"use strict";

class Table {

    constructor(aWidth, aLength) {
        this.width = aWidth;
        this.length = aLength;
    }
        
    hasPosition(aPosition) {
        var res = 
            ( (aPosition.x >= 0) && (aPosition.x < this.width) &&
              (aPosition.y >= 0) && (aPosition.y < this.length) );
        return res;
    }

}

module.exports = {Table};
