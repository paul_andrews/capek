//  Direction class for Capek application
//  By Paul Andrews November 2018
//

"use strict";

var CapekMath = require('./capek_math.js').CapekMath;

const COMPASSPOINTS = ["EAST", "NORTH", "WEST", "SOUTH"];

class Direction {

    static east()  { return new Direction("EAST"); }
    static north() { return new Direction("NORTH"); }
    static west()  { return new Direction("WEST"); }
    static south() { return new Direction("SOUTH"); }

    static createIfValid(aCompassPoint) {
        var idx = COMPASSPOINTS.indexOf(aCompassPoint);
        if ( idx < 0 )
            return null; 
        return new Direction(aCompassPoint);
    }
        
    constructor(aCompassPoint) {
        var idx = COMPASSPOINTS.indexOf(aCompassPoint);
        if ( idx < 0 )
            idx = 0;
        this.compassPoint = aCompassPoint;
        this.angle = idx * 90;
    }

    equals(aDir) {
        return (this.angle == aDir.angle);
    }

    getSign() {
        if ( (this.angle == 0) || (this.angle == 90) )
            return 1;
        else
            return -1;
    }

    isVertical() {
        return (this.angle == 90) || (this.angle == 270);
    }

    rotateLeft() {
        return this.rotateDegrees(90);
    }

    rotateRight() {
        return this.rotateDegrees(-90);
    }

    rotateDegrees(aDegrees) {
        var newAngle = CapekMath.modulo(this.angle + aDegrees, 360);
        var cp = this.angleToCompassPoint(newAngle);
        return new Direction(cp);
    }

    angleToCompassPoint(aAngle) {
        var idx = aAngle / 90;
        return COMPASSPOINTS[idx];
    }

}

module.exports = {Direction};
